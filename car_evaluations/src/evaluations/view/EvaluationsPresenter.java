package evaluations.view;

import evaluations.model.Evaluation;
import evaluations.model.Evaluations;
import javafx.scene.chart.XYChart;

import java.util.Map;

public class EvaluationsPresenter {
	private final Evaluations model;
	private final EvaluationsView view;
	private static final String USER_RATING_LABEL = "User Rating";
	private static final String MILEAGE_LABEL = "Mileage";
	private static final String SAFETY_LABEL = "Safety";

	public EvaluationsPresenter(Evaluations model, EvaluationsView view) {
		this.model = model;
		this.view = view;

		this.addEventHandlers();
		this.updateView();
	}

	private void addEventHandlers() {
	}

	private void updateView() {
	}
}

